#!/usr/bin/env python3

import socket

import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.ioloop
import tornado.iostream
import tornado.tcpserver


last_messages = dict()


class TcpClient(object):

    client_id = 0

    def __init__(self, stream):
        super().__init__()
        TcpClient.client_id += 1
        self.id = 'Connection#%d' % TcpClient.client_id
        self.stream = stream

        self.stream.socket.setsockopt(
            socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.stream.socket.setsockopt(
            socket.IPPROTO_TCP, socket.SO_KEEPALIVE, 1)
        self.stream.set_close_callback(self.on_disconnect)

    @tornado.gen.coroutine
    def on_disconnect(self):
        self.log("disconnected")
        yield []

    @tornado.gen.coroutine
    def dispatch_client(self):
        try:
            while True:
                line = yield self.stream.read_until(b'\n')

                text_line = line.decode('utf-8').strip()
                last_messages[self.id] = text_line

                self.log('got |%s|' % text_line)
        except tornado.iostream.StreamClosedError:
            pass

    @tornado.gen.coroutine
    def on_connect(self):
        raddr = 'closed'
        try:
            raddr = '%s:%d' % self.stream.socket.getpeername()
        except Exception:
            pass
        self.log('new, %s' % raddr)

        yield self.dispatch_client()

    def log(self, msg, *args, **kwargs):
        print('[%s] %s' % (self.id, msg.format(*args, **kwargs)))


class TcpServer(tornado.tcpserver.TCPServer):

    @tornado.gen.coroutine
    def handle_stream(self, stream, address):
        connection = TcpClient(stream)
        yield connection.on_connect()


class LastMessagesHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(last_messages)

def main():
    tcp = TcpServer()
    tcp.listen(8008)
    print('tcp://localhost:8008')

    app = tornado.web.Application([(r"/", LastMessagesHandler)])
    app.listen(8009)
    print("http://localhost:8009")

    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()

