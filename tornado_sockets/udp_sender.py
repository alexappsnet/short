#!/usr/bin/env python3

import socket

udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_sock.sendto('test1 msg'.encode(), ('localhost', 8008))
udp_sock.sendto('another msg'.encode(), ('localhost', 8008))
