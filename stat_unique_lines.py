#!/usr/bin/env python3

"""
Reads STDIN and counts repeating lines. Every second it prints the number of each kind of lines read from STDIN.

Example:
  If some app.py printed this during last second:

    update A
    update A
    update A
    update B
    update A
    update B

  then the output of "app.py | lines_rate.py" will be:

    2015-09-20 22:47:21.961750:
      update A: 4
      update B: 2

Note: the STDIN might be buffered. Sometimes it help to do "unbuffering":

> app.py | stdbuf -o0 grep "update" | python -u lines_rate.py
                 -----                      ---- 
"""

import threading
import time
import datetime
from multiprocessing import Lock
import sys


def print_stats(counter):
    while True:
        time.sleep(1)
        lines = counter.get()
        print('%s:' % datetime.datetime.now())
        for x in sorted(lines):
            print('  %s: %d' % (x, lines[x]))


class Counter(object):
    def __init__(self):
        self.lines = {}
        self.lock = Lock()

    def increment(self, string):
        with self.lock:
            if string in self.lines:
                self.lines[string] += 1
            else:
                self.lines[string] = 1

    def get(self):
        with self.lock:
            ret = self.lines
            self.lines = {}
            return ret


if __name__ == '__main__':
    counter = Counter()

    thread = threading.Thread(target = print_stats, args = (counter, ))
    thread.daemon = True
    thread.start()

    for line in sys.stdin:
        counter.increment(line.strip())
