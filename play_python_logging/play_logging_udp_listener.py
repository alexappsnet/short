#!/usr/bin/env python3

import socket
import pickle
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s:PID_%(process)d:%(message)s')
logger = logging.getLogger('UDP reader')

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('localhost', 10003)
logger.info('Starting listener on %s:%s' % server_address)
sock.bind(server_address)

while True:
    logger.debug('Waiting for a message')
    data, address = sock.recvfrom(65535)
    logger.debug('Received %s bytes from %s' % (len(data), address))

    if data:
        lr = logging.makeLogRecord(pickle.loads(data[4:]))
        logger.handle(lr)

