#!/usr/bin/env python3

import logging
import logging.config
import time


logging.config.fileConfig('play_logging.conf', disable_existing_loggers=False)
logger1 = logging.getLogger('component1')
logger2 = logging.getLogger('component2')


def main():
    while True: 
        logger1.info('Log#1')
        logger2.info('Log#2')
        logger1.debug('Log#3')

        time.sleep(5)


if __name__ == '__main__':
    main()

