#!/usr/bin/env python3
 
"""
There are 3 closed doors. One of the doors has the prize. You pick
the door to try to find the prize. WITHOUT opening the door that
you picked the other "empty" door opens for you. So, now there
are only 2 doors left to pick - the one that you already selected
initially and the other unopened door.

Question: Would your chance to win increase if you switch your
guess? Or should you just stay firm on your initial guess?


OUTPUT of this test:
--------------------------------------------------------------
Chances to get the prize if you...:
  don't switch: 0.329
  switch      : 0.671

"""

import random
 
PRIZE = 1
EMPTY = 0

COMBINATION1 = (PRIZE, EMPTY, EMPTY)
COMBINATION2 = (EMPTY, PRIZE, EMPTY)
COMBINATION3 = (EMPTY, EMPTY, PRIZE)

ALL_COMBINATIONS = (COMBINATION1, COMBINATION2, COMBINATION3)
 
def man_guess(switch):
    # randomly select on of 3 possible combinations with the doors
    combination = random.choice(ALL_COMBINATIONS)
 
    # randomly make a choice for the contestant
    guess = random.randint(0, 2)
 
    # find the empty door to open for contestant
    show = guess
    while show == guess or combination[show] == PRIZE:
        show = random.randint(0, 2)
 
    # switch or not
    if switch:
        guess = [x for x in range(3) if x != guess and x != show][0]
 
    # return the doors combination and the final pick from contestant
    return combination, guess
 
 
def check(game_count, switch):
    guesses = 0
    for i in range(game_count):
        combination, guess = man_guess(switch=switch)
        if combination[guess] == PRIZE:
            guesses += 1
 
    # return ratio of success
    return guesses/game_count
 
 
print("Chances to get the prize if you...:")
print("  don't switch:", check(1000, switch=False))
print("  switch      :", check(1000, switch=True))

