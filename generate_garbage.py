#!/usr/bin/env python3

import time
import random


if __name__ == '__main__':
    while True:
        time.sleep(random.randint(1, 100)/1000.0)
        print(time.time(), random.choice(['A', 'B', 'C']), flush=True)
