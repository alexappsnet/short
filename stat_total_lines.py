#!/usr/bin/env python3

"""
Reads STDIN and counts lines. Every second it prints the number of lines read from STDIN.

Note: the STDIN might be buffered. Sometimes it help to do "unbuffering":

> my_app.exe | stdbuf -o0 grep "update" | python -u line_rate.py
              ------------                      ---- 
"""

import threading
import time
import datetime
from multiprocessing import Lock
import sys


def print_stats(counter):
    while True:
        time.sleep(1)
        print('%s: %d' % (datetime.datetime.now(), counter.set(0)))


class Counter(object):
    def __init__(self):
        self.val = 0
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self.val += 1

    def set(self, new_value):
        with self.lock:
            ret = self.val
            self.val = new_value
            return ret


if __name__ == '__main__':
    counter = Counter()

    thread = threading.Thread(target = print_stats, args = (counter, ))
    thread.daemon = True
    thread.start()

    for line in sys.stdin:
        counter.increment()
